package com.example.myapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button2.setOnClickListener {
            if (editTextTextEmailAddress.text.isNullOrBlank() && editTextTextPassword.text.isNullOrBlank()) {
                Toast.makeText(this, "please fill the required fields", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(
                    this,
                    "${editTextTextEmailAddress.text} Logged in Successfully",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}

